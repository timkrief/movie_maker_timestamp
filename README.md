# Movie Maker Timestamp

![Movie Maker Timestamp Icon](icon.png "Movie Maker Timestamp Icon")

This is a simple plugin to automatically add a timestamp to your recordings. That way, you shouldn't rewrite old recordings by accident.

# Install

To use the plugin in your own project you can copy all the files in `addons/` to the 
`addons/` folder of your project and enable the plugin in Godot. Complete 
instructions are available here: [Installing plugins (GodotDocs)](https://docs.godotengine.org/en/stable/tutorials/plugins/editor/installing_plugins.html)

# How to use

After enabling the plugin, close the project settings. Now if you open project settings again, the "movie file" should now be replaced with "movie file without timestamp" under "editor/movie_writer/". You can specify where the file should be saved as you normally do, but now when you record, it saves to the file name specified plus a timestamp.

For instance "movie.avi" is instead saved as "movie-2024-04-19_16.36.13.avi".
